<br />
<div align="center">
  <h2 align="center">Urban Ease</h2>

  <p align="center">
   Clothing e-commerce application with cart management functionality
    <br />
  </p>
  <a href="https://urban-ease.vercel.app/"><strong>Live site here »</strong></a>


</div>
 <br  />
 <br  />

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

### Built With

#### Frontend

- React
- TypeScript
- Redux Toolkit
- Tanstack Query
- React Router
- Tailwind

#### Deployment

- Vercel - Frontend Hosting

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

**Cart Feature**

1. Navigate to [urban-ease.vercel.app](https://urban-ease.vercel.app/)
2. Navigate to men's or women's shopping section
3. Select a clothing item + size
4. Click add to cart
5. Navigate to the shopping cart by clicking on the cart icon in the top right
6. Edit quantity or delete item as necessary

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->

## Contact

Robbie Tambunting

Website: [www.robbietambunting.com](https://www.robbietambunting.com) <br/>
LinkedIn: [https://www.linkedin.com/in/robbie-tambunting/](https://www.linkedin.com/in/robbie-tambunting/) <br/>

Live Site: [Urban Ease](https://urban-ease.vercel.app/) <br/>
Project Link: [Urban Ease Repo](https://gitlab.com/rtambunt/urban-ease)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

Fake Store API for data:   <a href="https://fakestoreapi.com/">fakestoreapi.com</a>
