/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      gridTemplateColumns: {
        responsive: "repeat(auto-fill, minmax(22rem, 1fr))",
      },
    },
  },
  plugins: [],
};
