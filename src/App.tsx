import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

import AppLayout from "./ui/AppLayout";
import Error from "./ui/Error";
import Home from "./ui/Home";
import Mens from "./features/mens clothing/Mens";
import Womens from "./features/womens clothing/Womens";
import MensItem from "./features/mens clothing/MensItem";
import WomensItem from "./features/womens clothing/WomensItem";
import Cart from "./features/cart/Cart";
import { Toaster } from "react-hot-toast";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 0,
    },
  },
});

const router = createBrowserRouter([
  {
    element: <AppLayout />,
    errorElement: <Error />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/mens",
        element: <Mens />,
      },
      {
        path: "/mens/:itemId",
        element: <MensItem />,
      },
      {
        path: "/womens",
        element: <Womens />,
      },
      {
        path: "/womens/:itemId",
        element: <WomensItem />,
      },
      {
        path: "/cart",
        element: <Cart />,
      },
    ],
  },
]);

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
      <Toaster
        position="top-center"
        toastOptions={{
          success: {
            duration: 3000,
          },
          error: {
            duration: 5000,
          },
          style: {
            padding: "1rem 3rem",
          },
        }}
      />
    </QueryClientProvider>
  );
}

export default App;
