import { APIClothingItem } from "./types/apiTypes";

const apiUrl = "https://fakestoreapi.com";

export async function getMensInventory(): Promise<APIClothingItem[]> {
  const res = await fetch(`${apiUrl}/products/category/men's clothing`);

  if (!res.ok) throw new Error("Failed to fetch men's inventory products");

  const data = await res.json();

  return data;
}

export async function getWomensInventory(): Promise<APIClothingItem[]> {
  const res = await fetch(`${apiUrl}/products/category/women's clothing`);

  if (!res.ok) throw new Error("Failed to fetch women's inventory products");

  const data = await res.json();

  return data;
}

export async function getItem(itemId: string | undefined) {
  const res = await fetch(`${apiUrl}/products/${itemId}`);

  if (!res.ok) throw new Error("Failed to fetch single item");

  const data = await res.json();

  return data;
}
