import { getCart, getCartQuantity, getCartTotalPrice } from "./cartSlice";
import CartItem from "./CartItem";
import toast from "react-hot-toast";
import { useAppSelector } from "../../types/reduxTypes";

function Cart() {
  const cart = useAppSelector(getCart);
  const totalPrice = useAppSelector(getCartTotalPrice);
  const quantity = useAppSelector(getCartQuantity);

  console.log(cart);

  return (
    <div>
      <h1 className="mb-8 text-2xl lg:mb-12 lg:text-3xl xl:text-center">
        Shopping Cart
      </h1>

      <div className="gap-x-5 xl:flex xl:flex-row-reverse xl:justify-center">
        <div className="mb-5 flex items-center justify-between rounded-lg border p-4 xl:h-44 xl:flex-col xl:items-start xl:px-8 xl:pb-9 xl:pt-7">
          <div className="flex flex-col gap-y-2">
            <div className="text-nowrap text-sm font-normal uppercase md:text-base">
              {quantity} {quantity === 1 ? "item" : "items"}
            </div>
            <h2 className="font-semibold md:text-lg">Total: {totalPrice}</h2>
          </div>

          <button
            className="rounded-lg border-4 border-lime-500 bg-lime-500 px-12 py-1 text-slate-50 transition-colors duration-300 hover:border-lime-700 hover:bg-lime-700"
            onClick={() =>
              toast(
                "This is an example project with fake products. Checkout is not available!",
                { icon: "❌" },
              )
            }
          >
            Checkout
          </button>
        </div>

        <div className="flex flex-col gap-y-4">
          {cart.map((item) => (
            <CartItem key={item.title + item.size} item={item} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default Cart;
