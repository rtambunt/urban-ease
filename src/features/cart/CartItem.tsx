import { FaTrashAlt } from "react-icons/fa";
import toast from "react-hot-toast";

import { incItemQuantity, decItemQuantity, deleteItem } from "./cartSlice";
import { useAppDispatch } from "../../types/reduxTypes";
import type { CartItem } from "../../types/appTypes";

type Props = {
  item: CartItem;
};

function CartItem({ item }: Props) {
  const dispatch = useAppDispatch();

  return (
    <div className="relative flex items-center justify-between gap-x-4 rounded-lg border bg-white p-10 text-xs sm:text-sm md:text-base">
      <div>
        <h2 className="mb-2 font-semibold md:text-lg">{item.title}</h2>
        <div className="mb-12 md:mb-52">size: {item.size}</div>

        <div className="mb-4 flex items-center gap-x-4 ">
          <button
            onClick={() => {
              dispatch(deleteItem(item.id));
              toast("Item Deleted", { icon: "🗑️" });
            }}
          >
            <FaTrashAlt className="mr-2 text-red-500 transition-all duration-300 hover:-translate-y-1" />
          </button>

          <button
            className="h-full rounded-lg border px-3 py-1 transition-colors duration-200 hover:bg-slate-200"
            onClick={() => dispatch(decItemQuantity(item.id))}
          >
            -
          </button>
          <div>{item.quantity}</div>
          <button
            className="h-full rounded-lg border px-3 py-1 transition-colors duration-200 hover:bg-slate-200"
            onClick={() => dispatch(incItemQuantity(item.id))}
          >
            +
          </button>
        </div>
        <div className="absolute bottom-5 font-semibold sm:text-base lg:text-lg">
          ${item.price}
        </div>
      </div>

      <div>
        <img
          className="mx-auto max-w-20 sm:max-w-40 md:max-w-56"
          src={item.image}
          alt={item.title + " Photo"}
        />
      </div>
    </div>
  );
}

export default CartItem;
