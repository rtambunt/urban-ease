import { createSlice } from "@reduxjs/toolkit";

import { formatUSD } from "../../helpers";
import { RootState } from "../../store";
import type { InitialState, CartItem } from "../../types/appTypes";

const initialState: InitialState = {
  cart: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addItem(state, action) {
      const newItem = action.payload;
      const duplicateInCart = state.cart.find(
        (item: CartItem) =>
          item.id === newItem.id && item.size === newItem.size,
      );

      if (duplicateInCart === undefined) {
        state.cart.push(action.payload);
      } else {
        duplicateInCart.quantity += newItem.quantity;
      }
    },
    deleteItem(state, action) {
      state.cart = state.cart.filter(
        (item: CartItem | undefined) => item?.id !== action.payload,
      );
    },
    incItemQuantity(state, action) {
      const item = state.cart.find(
        (item: CartItem | undefined) => item?.id === action.payload,
      );

      if (item !== undefined) item["quantity"]++;
    },
    decItemQuantity(state, action) {
      const item = state.cart.find(
        (item: CartItem | undefined) => item?.id === action.payload,
      );
      if (item === undefined) return;

      item["quantity"]--;

      if (item["quantity"] <= 0)
        cartSlice.caseReducers.deleteItem(state, action);
    },
  },
});

export const { addItem, deleteItem, incItemQuantity, decItemQuantity } =
  cartSlice.actions;

export default cartSlice.reducer;

export const getCart = (state: RootState) => state.cart.cart;

export const getCartTotalPrice = (state: RootState) => {
  const total = state.cart.cart.reduce(
    (total, item) => total + item.price * item.quantity,
    0,
  );

  const formattedTotal = formatUSD(Math.round(total * 100) / 100);

  return formattedTotal;
};

export const getCartQuantity = (state: RootState) =>
  state.cart.cart.reduce((quantity, item) => (quantity += item.quantity), 0);
