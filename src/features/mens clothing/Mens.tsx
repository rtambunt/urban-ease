import { useQuery } from "@tanstack/react-query";

import { getMensInventory } from "../../apiInventory";
import ItemDisplay from "../../ui/ItemDisplay";

function Mens() {
  const { isLoading, data: mensInventory } = useQuery({
    queryKey: ["mens"],
    queryFn: getMensInventory,
  });

  if (isLoading) return <p>Loading...</p>;

  return (
    <>
      <h1 className="mb-8 text-2xl lg:text-3xl xl:mb-12 xl:text-center">
        Men's Clothing
      </h1>

      <div className="grid grid-cols-1 gap-x-6 gap-y-5 md:grid-cols-responsive ">
        {mensInventory?.map((item) => (
          <ItemDisplay item={item} type="mens" key={item.title} />
        ))}
      </div>
    </>
  );
}

export default Mens;
