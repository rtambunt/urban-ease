import { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { useParams } from "react-router-dom";

import { getItem } from "../../apiInventory";
import SizeButton from "../../ui/SizeButton";
import { addItem } from "../cart/cartSlice";
import { formatUSD } from "../../helpers";
import toast from "react-hot-toast";
import { useAppDispatch } from "../../types/reduxTypes";

function WomensItem() {
  const { itemId } = useParams();
  const dispatch = useAppDispatch();

  const { isLoading, data: item } = useQuery({
    queryKey: ["womens_item"],
    queryFn: () => getItem(itemId),
  });

  const [quantity, setQuantity] = useState(1);
  const [size, setSize] = useState("S");

  function handleIncQuantity() {
    setQuantity((prev) => prev + 1);
  }

  function handleDecQuantity() {
    if (quantity > 0) setQuantity((prev) => prev - 1);
  }

  function handleAddToCart() {
    const newItem = {
      id: item.id,
      title: item.title,
      price: item.price,
      image: item.image,
      size,
      quantity,
    };

    dispatch(addItem(newItem));

    toast("Added to Cart!", { icon: "✅" });
  }

  if (isLoading) return <p>Loading...</p>;

  return (
    <div className="flex flex-col items-center rounded-lg bg-white px-14 py-16 md:text-left lg:flex-row lg:gap-x-40">
      <img
        className=" mb-20 max-w-44 md:max-w-72 lg:mb-0 lg:ml-16 xl:max-w-96"
        src={item.image}
        alt={item.title + " Photo"}
      />

      <div className="text-center lg:text-start">
        <p className="text-xl md:text-3xl">{item.title}</p>
        <p className="mt-5 text-lg">{formatUSD(item.price)}</p>

        <p className="mt-10 text-sm">Size:</p>
        <div className="mt-2 flex items-center justify-center gap-x-1 lg:justify-start">
          <SizeButton
            onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
              const target = e.target as HTMLElement;
              setSize(target.innerText);
            }}
            sizeId={"XS"}
            curSize={size}
          >
            XS
          </SizeButton>
          <SizeButton
            onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
              const target = e.target as HTMLElement;
              setSize(target.innerText);
            }}
            sizeId={"S"}
            curSize={size}
          >
            S
          </SizeButton>
          <SizeButton
            onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
              const target = e.target as HTMLElement;
              setSize(target.innerText);
            }}
            sizeId={"M"}
            curSize={size}
          >
            M
          </SizeButton>
          <SizeButton
            onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
              const target = e.target as HTMLElement;
              setSize(target.innerText);
            }}
            sizeId={"L"}
            curSize={size}
          >
            L
          </SizeButton>
          <SizeButton
            onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
              const target = e.target as HTMLElement;
              setSize(target.innerText);
            }}
            sizeId={"XL"}
            curSize={size}
          >
            XL
          </SizeButton>
        </div>

        <div className="mt-12">
          <label htmlFor="quantity" className="text-sm">
            Quantity:
          </label>
          <div className="mt-2 h-10">
            <button
              className="h-full border px-4 transition-colors duration-200 hover:bg-slate-200"
              onClick={() => handleDecQuantity()}
            >
              -
            </button>
            <input
              className="mx-2 h-full w-20 appearance-none border text-center outline-slate-300"
              id="quantity"
              type="number"
              value={quantity === 0 ? "" : quantity}
              onChange={(e) => setQuantity(Number(e.target.value))}
            />
            <button
              className="h-full border px-4 transition-colors duration-200 hover:bg-slate-200"
              onClick={() => handleIncQuantity()}
            >
              +
            </button>
          </div>
        </div>

        <button
          className="mt-12 w-full border py-5 uppercase tracking-wider transition-colors duration-200 hover:bg-slate-800 hover:text-slate-100"
          onClick={handleAddToCart}
        >
          Add to Cart
        </button>
        <p className="talic mt-16 text-slate-600">{item.description}</p>
      </div>
    </div>
  );
}

export default WomensItem;
