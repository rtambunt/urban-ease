import { useQuery } from "@tanstack/react-query";

import { getWomensInventory } from "../../apiInventory";
import ItemDisplay from "../../ui/ItemDisplay";

function Womens() {
  const { isLoading, data: womensInventory } = useQuery({
    queryKey: ["womens"],
    queryFn: getWomensInventory,
  });

  if (isLoading) return <p>Loading...</p>;

  return (
    <>
      <h1 className="mb-8 text-2xl lg:text-3xl xl:mb-12 xl:text-center">
        Women's Clothing
      </h1>

      <div className="grid grid-cols-1 gap-x-6 gap-y-5 md:grid-cols-responsive ">
        {womensInventory?.map((item) => (
          <ItemDisplay item={item} type="womens" key={item.title} />
        ))}
      </div>
    </>
  );
}

export default Womens;
