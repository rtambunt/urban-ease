import { Link } from "react-router-dom";
import { IoCartOutline } from "react-icons/io5";
import { useState } from "react";

import { getCartQuantity } from "../../features/cart/cartSlice";
import HamburgerIcon from "./HamburgerIcon";
import HamburgerOptions from "./HamburgerOptions";
import { useAppSelector } from "../../types/reduxTypes";

function Header() {
  const cartQuantity = useAppSelector(getCartQuantity);

  const [isHamburgerActive, setIsHamburgerActive] = useState(false);

  function handleIsHamburgerActive() {
    setIsHamburgerActive((prev) => !prev);
  }

  return (
    <>
      <HamburgerOptions isHamburgerActive={isHamburgerActive} />
      <div className="relative z-50 flex items-center justify-between bg-white px-4 pb-8 md:px-8">
        <HamburgerIcon
          isHamburgerActive={isHamburgerActive}
          handleIsHamburgerActive={handleIsHamburgerActive}
        />

        <span className="relative -top-1 whitespace-nowrap text-4xl transition-all duration-300 hover:-translate-y-1  hover:text-slate-600">
          <Link to="/">urban ease</Link>
        </span>

        <div className="hidden w-56 items-center justify-around pt-1 md:flex lg:text-lg">
          <Link
            to="/mens"
            className="transition-all duration-300 hover:-translate-y-1  hover:text-slate-600"
          >
            men
          </Link>
          <Link
            to="/womens"
            className="transition-all duration-300 hover:-translate-y-1  hover:text-slate-600"
          >
            women
          </Link>
        </div>

        <div className="relative transition-all duration-300 hover:-translate-y-1 hover:text-slate-600">
          <Link to="/cart" className="px-7 py-2">
            <IoCartOutline size="1.5rem" />
          </Link>
          {cartQuantity > 0 && (
            <div className="absolute bottom-3 left-3 rounded-full bg-lime-500 px-2 py-[.2rem] text-xs text-slate-800">
              {cartQuantity}
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default Header;
