import { Link } from "react-router-dom";
import { motion } from "framer-motion";

const HamburgerOptions = ({
  isHamburgerActive,
}: {
  isHamburgerActive: boolean;
}) => {
  return (
    <motion.div
      initial={false}
      animate={isHamburgerActive ? "open" : "closed"}
      transition={{ type: "spring" }}
      variants={{
        open: {
          y: -10,
        },
        closed: {
          y: -50,
        },
      }}
      className="absolute top-32 z-20 w-full rounded-b-xl bg-slate-300 pb-4 pt-7 sm:text-lg md:hidden"
    >
      <div
        className={`flex flex-col items-center justify-center divide-y divide-stone-300 overflow-hidden  transition-colors ${isHamburgerActive ? "" : "hidden"}`}
      >
        <Link
          className="px-3 py-2  duration-300 hover:-translate-y-1 hover:text-stone-400 focus:outline-stone-600"
          to="/mens"
        >
          Mens
        </Link>
        <Link
          className="px-3  py-2  duration-300 hover:-translate-y-1 hover:text-stone-400 focus:outline-stone-600"
          to="/womens"
        >
          Womens
        </Link>
      </div>
    </motion.div>
  );
};

export default HamburgerOptions;
