import { Link } from "react-router-dom";

function Home() {
  return (
    <div className="h-[29rem] sm:h-[38rem]">
      <div className="relative flex h-full w-full justify-center px-5 lg:h-5/6">
        <div className="absolute left-0 top-0 h-full w-full rounded-3xl bg-gradient-to-br from-blue-100/80 to-blue-400 object-cover"></div>
        <img
          className="absolute left-0 top-0 h-full w-full rounded-3xl object-cover opacity-80"
          src="/images/pexels-urban-ease-home.jpg"
          alt="Urban Ease Home Photo"
        />
        <div className="absolute bottom-2 z-20 rounded-2xl bg-slate-50 px-4 py-4 text-left sm:bottom-3 sm:right-4 md:px-8 lg:px-10 lg:py-6">
          <h1 className=" mb-1 text-2xl font-bold sm:text-3xl lg:mb-2 lg:text-4xl">
            Simply Modern.
          </h1>
          <p className="mb-4 text-xs sm:text-sm lg:mb-8 lg:text-base">
            Minimalistic pieces for everyday wear
          </p>
          <Link to="/mens">
            <button className="mr-1 rounded-full border bg-slate-600 px-5 py-2 text-sm font-semibold text-slate-100 transition-colors duration-200 hover:bg-slate-400 sm:px-7 sm:text-base lg:text-lg">
              Shop Mens
            </button>
          </Link>

          <Link to="/womens">
            <button className="rounded-full border bg-slate-600 px-5 py-2 text-sm font-semibold text-slate-100 transition-colors duration-200 hover:bg-slate-400 sm:px-7 sm:text-base lg:text-lg">
              Shop Womens
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Home;
