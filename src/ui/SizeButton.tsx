// Add onClick later for adding size to shopping cart !

type Props = {
  children: React.ReactNode;
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  sizeId: number | string;
  curSize: string;
};

function SizeButton({ children, onClick, sizeId, curSize }: Props) {
  const defaultStyles =
    "rounded-lg px-4 py-2 outline-slate-800 transition-colors duration-300 ";

  if (curSize === sizeId)
    return (
      <button
        className={`${defaultStyles} bg-slate-800 text-slate-100`}
        autoFocus
        onClick={onClick}
      >
        {children}
      </button>
    );

  // --- Used for tailwind emmet snippet (Delete later !) ---
  // <button className="rounded-lg px-4 py-2 outline-slate-300 transition-colors duration-300 hover:bg-slate-200 focus:bg-slate-800 focus:text-slate-100"></button>;

  // return (
  //   <button className={`${defaultStyles} hover:bg-slate-200`} onClick={onClick}>
  //     {children}
  //   </button>
  // );

  return (
    <button className={`${defaultStyles} hover:bg-slate-200`} onClick={onClick}>
      {children}
    </button>
  );
}

export default SizeButton;
