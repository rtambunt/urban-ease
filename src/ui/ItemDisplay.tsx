import { Link } from "react-router-dom";

import { formatUSD } from "../helpers";
import { APIClothingItem } from "../types/apiTypes";

type Props = {
  item: APIClothingItem;
  type: string;
};

function ItemDisplay({ item, type }: Props) {
  const itemRoute = type === "mens" ? `/mens/${item.id}` : `/womens/${item.id}`;

  return (
    <div className="flex flex-col items-center rounded-md border bg-white px-8 pb-6 pt-10 text-center">
      <Link to={itemRoute}>
        <img className="h-72 " src={item.image} alt={item.title + " photo"} />
      </Link>
      <Link className="mt-12" to={itemRoute}>
        <p className="font-semibold  transition-colors duration-150 hover:text-slate-500">
          {item.title}
        </p>
      </Link>
      <p className="mt-2 line-clamp-2 text-sm text-slate-600">
        {item.description}
      </p>
      <Link className="mt-6" to={itemRoute}>
        <p className="font-semibold">{formatUSD(item.price)}</p>
      </Link>
    </div>
  );
}

export default ItemDisplay;
