import { Outlet } from "react-router-dom";
import Header from "./header/Header";

function AppLayout() {
  return (
    <div className="pt-8">
      <Header />
      <main
        className="h-auto min-h-[29rem]
       bg-slate-50 px-10 pb-10 pt-6 md:px-20 md:pt-12 lg:pb-12"
      >
        <Outlet />
      </main>
    </div>
  );
}

export default AppLayout;
