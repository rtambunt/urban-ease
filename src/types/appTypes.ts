export type CartItem = {
  id: string;
  title: string;
  price: number;
  image: string;
  size: string;
  quantity: number;
};

export type InitialState = {
  cart: CartItem[];
};
